package com.sangeorgescout.motkdm;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.sangeorgescout.motkdm.Splash.URL;

public class AttendAdapter extends BaseAdapter{

    ArrayList<BoysModel> boys;
    Context context;

    public AttendAdapter(ArrayList<BoysModel> boys, Context context) {
        this.boys = boys;
        this.context = context;
    }

    @Override
    public int getCount() {
        return boys.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.attend_list_item,null);
        TextView name = convertView.findViewById(R.id.name);
        final CheckBox attend = convertView.findViewById(R.id.attend);
        name.setText(boys.get(position).name);
        attend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    addAttend(boys.get(position).name,boys.get(position).id,attend);
            }
        });
        ImageView exit = convertView.findViewById(R.id.exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPermDialog(boys.get(position).name,boys.get(position).id);
            }
        });
        return convertView;
    }

    public void showPermDialog(final String name, final String id)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.add_fees_dialog,null);
        final AlertDialog dialog = new AlertDialog.Builder(context).create();

        final EditText perm = dialogView.findViewById(R.id.amount);
        perm.setHint("العذر");
        perm.setInputType(InputType.TYPE_CLASS_TEXT);
        final ImageView addAmount = dialogView.findViewById(R.id.add);
        final ImageView cancel = dialogView.findViewById(R.id.cancel);

        addAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAmount.startAnimation(Main.buttonClick);
                addPerm(name,id,perm.getText().toString());
                dialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel.startAnimation(Main.buttonClick);
                dialog.cancel();
            }
        });

        dialog.setView(dialogView);
        dialog.show();
    }

    public void addAttend(final String name, final String id, final CheckBox checkBox) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = URL+"addAttend.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        if (response.contains("true")) {
                            Toast.makeText(context, "تم التسجيل", Toast.LENGTH_SHORT).show();
                            checkBox.setEnabled(false);
                        }
                        else if (response.contains("Duplicate")) {
                            Toast.makeText(context, "الفرد مسجل", Toast.LENGTH_SHORT).show();
                            checkBox.setChecked(false);
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("boy_id", id);
                params.put("boy_name",name);
                params.put("attend_date", AttendAdd.attDate.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void addPerm(final String name, final String id, final String perm) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = URL+"addPerm.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        if (response.contains("true")) {
                            Toast.makeText(context, "تم التسجيل", Toast.LENGTH_SHORT).show();
                        }
                        else if (response.contains("Duplicate")) {
                            Toast.makeText(context, "العذر مسجل", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("boy_id", id);
                params.put("boy_name",name);
                params.put("perm",perm);
                params.put("perm_date", AttendAdd.attDate.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }
}
