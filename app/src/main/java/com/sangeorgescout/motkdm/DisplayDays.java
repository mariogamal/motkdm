package com.sangeorgescout.motkdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class DisplayDays extends AppCompatActivity {

    public static String id;
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_days);
        list = findViewById(R.id.daysList);
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"getDates.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        fetchResult(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id",id);
                return params;
            }
        };
        queue.add(postRequest);
    }


    public void fetchResult(String result){
        try
        {
            ArrayList<String> dates = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String attend_date = object.getString("attend_date");
                dates.add(attend_date);
            }
            populateList(dates);

        }catch (Exception ex)
        {
            Toast.makeText(this, "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<String>());
        }
    }

    public void populateList(ArrayList<String> dates)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.boy_list_item, android.R.id.text1, dates);
        list.setAdapter(adapter);
    }
}
