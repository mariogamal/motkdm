package com.sangeorgescout.motkdm;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class EditBoys extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener  {

    EditText name , phone , mobile , address, notes;
    TextView birthdate;
    RadioGroup grade;
    RadioButton first,second,third;
    public static BoysModel boy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_boys);
        name = findViewById(R.id.name);
        name.setText(boy.name);
        phone = findViewById(R.id.phone);
        phone.setText(boy.phone);
        mobile = findViewById(R.id.mobile);
        mobile.setText(boy.mobile);
        address = findViewById(R.id.address);
        address.setText(boy.address);
        notes = findViewById(R.id.notes);
        notes.setText(boy.notes);
        birthdate = findViewById(R.id.birthdate);
        birthdate.setText(boy.birthdate);
        grade = findViewById(R.id.grade);
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        third = findViewById(R.id.third);
        switch (boy.grade){
            case "1":
                first.setChecked(true);
                break;

            case "2":
                second.setChecked(true);
                break;

            case "3":
                third.setChecked(true);
                break;
        }
    }

    public void openPicker(View view) {
        DatePickerDialog dialog = new DatePickerDialog(this, this, 2000, 0, 1);
        dialog.show();
    }

    public void editMember(View view) {
        view.startAnimation(Main.buttonClick);
        sendData();
    }

    public void sendData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"editBoys.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if(response.contains("true")) {
                            Toast.makeText(getApplicationContext(), "تم تعديل البيانات", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id", boy.id);
                params.put("name", name.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("mobile", mobile.getText().toString());
                params.put("birthdate", birthdate.getText().toString());
                params.put("address",  address.getText().toString());
                params.put("notes",  notes.getText().toString());
                params.put("grade", findViewById(grade.getCheckedRadioButtonId()).getTag().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        birthdate.setText(year+"/"+(month+1)+"/"+dayOfMonth);
    }
}
