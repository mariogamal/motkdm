package com.sangeorgescout.motkdm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

public class Main extends AppCompatActivity {

    //private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
    public static final ScaleAnimation buttonClick = new ScaleAnimation(1.0f, 1.0f, 0.9f, 0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //buttonClick = new AlphaAnimation(1F, 0.8F);
    }

    public void openAddMember(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,AddBoy.class));
    }

    public void openGetBoys(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,GetBoys.class));
    }

    public void openBoysFees(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,BoysFees.class));
    }

    public void openAttendAdd(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,AttendAdd.class));
    }

    public void openDayAttendance(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,DayAttendance.class));
    }

    public void openBoyAttendance(View view) {
        view.startAnimation(buttonClick);
        startActivity(new Intent(this,BoyAttendance.class));
    }
}
