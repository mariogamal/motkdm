package com.sangeorgescout.motkdm;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class BoysFees extends AppCompatActivity {

    ListView list;
    ArrayList<BoysModel> mainBoys;
    EditText filterBoys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boys_fees);
        list = findViewById(R.id.boysList);
        mainBoys = new ArrayList<>();
        filterBoys = findViewById(R.id.searchText);
        filterBoys.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) {
                searchNfilter(s.toString());
            }
        });
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"getBoys.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        fetchResult(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("condition", "");
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void fetchResult(String result){
        try
        {
            mainBoys = new ArrayList<>();
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String id = object.getString("id");
                String name = object.getString("name");
                String phone = object.getString("phone");
                String mobile = object.getString("mobile");
                String address = object.getString("address");
                String grade = object.getString("grade");
                String birthdate = object.getString("birthdate");
                String notes = object.getString("notes");
                String image = object.getString("img");
                String fees = object.getString("fees");

                BoysModel boy = new BoysModel(id,name,phone,mobile,birthdate,grade,address,notes,image,fees);
                mainBoys.add(boy);
                names.add(name+"\n"+fees+" جنية ");
            }
            populateList(names,mainBoys);

        }catch (Exception ex)
        {
            Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void searchNfilter(String key){
        ArrayList<BoysModel>boys = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        for (BoysModel boy : mainBoys){
            if (boy.name.contains(key))
            {
                boys.add(boy);
                names.add(boy.name+"\n"+boy.fees+" جنيه ");
            }
        }
        populateList(names,boys);
    }

    public void populateList(ArrayList<String> names, final ArrayList<BoysModel> boys)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.boy_list_item, android.R.id.text1, names);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getAmount(boys.get(position).id,boys.get(position).fees);
            }
        });
    }

    public void addFees(final String id, final String fees) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"addFees.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        if (response.contains("true")) {
                            Toast.makeText(BoysFees.this, "تم اضافة الاشتراك", Toast.LENGTH_SHORT).show();
                            getData();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("id", id);
                params.put("fees", fees);
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void getAmount(final String id ,final String fees){
        View dialogView = getLayoutInflater().inflate(R.layout.add_fees_dialog,null);
        final AlertDialog dialog = new AlertDialog.Builder(this).create();

        final EditText amount = dialogView.findViewById(R.id.amount);
        amount.setText(fees);
        final ImageView addAmount = dialogView.findViewById(R.id.add);
        final ImageView cancel = dialogView.findViewById(R.id.cancel);

        addAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAmount.startAnimation(Main.buttonClick);
                addFees(id,amount.getText().toString());
                dialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel.startAnimation(Main.buttonClick);
                dialog.cancel();
            }
        });

        dialog.setView(dialogView);
        dialog.show();
    }
}
