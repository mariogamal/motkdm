package com.sangeorgescout.motkdm;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class AddBoy extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    EditText name , phone , mobile , address, notes;
    TextView birthdate;
    RadioGroup grade;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_boy);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        mobile = findViewById(R.id.mobile);
        address = findViewById(R.id.address);
        notes = findViewById(R.id.notes);
        birthdate = findViewById(R.id.birthdate);
        grade = findViewById(R.id.grade);
    }

    public void addMember(View view) {
        view.startAnimation(Main.buttonClick);
        sendData();
    }

    public void sendData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"register.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if(response.contains("true"))
                            cleanFields();
                        else
                            Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("name", name.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("mobile", mobile.getText().toString());
                params.put("birthdate", birthdate.getText().toString());
                params.put("address",  address.getText().toString());
                params.put("notes",  notes.getText().toString());
                params.put("grade", findViewById(grade.getCheckedRadioButtonId()).getTag().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void cleanFields(){
        Toast.makeText(AddBoy.this, "تمت الاضافة", Toast.LENGTH_SHORT).show();
        name.setText("");
        phone.setText("");
        mobile.setText("");
        address.setText("");
        notes.setText("");
    }

    public void openPicker(View view) {
        DatePickerDialog dialog = new DatePickerDialog(this, this, 2000, 0, 1);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        birthdate.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
    }
}
