package com.sangeorgescout.motkdm;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class AttendAdd extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    ListView list;
    ArrayList<BoysModel> mainBoys;
    EditText filterBoys;
    public static TextView attDate;
    RadioButton first,second,third,all;
    RadioGroup grade;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend_add);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);

        attDate = findViewById(R.id.attDate);
        attDate.setText(year+"/"+month+"/"+day);

        list = findViewById(R.id.boysList);
        mainBoys = new ArrayList<>();
        filterBoys = findViewById(R.id.searchText);
        filterBoys.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) {
                searchNfilter(s.toString());
            }
        });
        grade = findViewById(R.id.grade);
        grade.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                getData();
            }
        });
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        third = findViewById(R.id.third);
        all = findViewById(R.id.all);
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"getBoys.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        fetchResult(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("condition", getCondition());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public String getCondition(){
        String cond = findViewById(grade.getCheckedRadioButtonId()).getTag().toString();
        switch (cond){
            case "0":
                return "";
            case "1":
                return "where grade = 1";
            case "2":
                return "where grade = 2";
            case "3":
                return "where grade = 3";
        }
        return "";
    }

    public void fetchResult(String result){
        mainBoys = new ArrayList<>();
        try
        {
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String id = object.getString("id");
                String name = object.getString("name");
                String phone = object.getString("phone");
                String mobile = object.getString("mobile");
                String address = object.getString("address");
                String grade = object.getString("grade");
                String birthdate = object.getString("birthdate");
                String notes = object.getString("notes");
                String image = object.getString("img");
                String fees = object.getString("fees");

                BoysModel boy = new BoysModel(id,name,phone,mobile,birthdate,grade,address,notes,image,fees);
                mainBoys.add(boy);
            }
            populateList(mainBoys);

        }catch (Exception ex)
        {
            //Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<BoysModel>());
        }
    }

    public void searchNfilter(String key){
        ArrayList<BoysModel>boys = new ArrayList<>();
        for (BoysModel boy : mainBoys){
            if (boy.name.contains(key))
            {
                boys.add(boy);
            }
        }
        populateList(boys);
    }

    public void populateList(final ArrayList<BoysModel> boys)
    {
        AttendAdapter adapter = new AttendAdapter(boys,this);
        list.setAdapter(adapter);
    }

    public void openPicker(View view) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this, this, year, month, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        attDate.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
    }
}
