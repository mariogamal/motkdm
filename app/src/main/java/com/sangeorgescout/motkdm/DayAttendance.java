package com.sangeorgescout.motkdm;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.sangeorgescout.motkdm.Splash.URL;

public class DayAttendance extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener{

    TextView attDate;
    ListView boysList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_attendance);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        attDate = findViewById(R.id.attDate);
        attDate.setText(year+"/"+month+"/"+day);
        boysList = findViewById(R.id.boysList);
        getData();

    }

    public void openPicker(View view) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this, this, year, month, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        attDate.setText(year+"/"+(monthOfYear+1)+"/"+dayOfMonth);
        getData();
    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"getAttendees.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        fetchResult(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("att_date",attDate.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void fetchResult(String result){
        try
        {
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(result);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("boy_name");
                names.add(name);
            }
            populateList(names);

        }catch (Exception ex)
        {
            Toast.makeText(this, "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<String>());
        }
    }

    public void populateList(ArrayList<String> names)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.boy_list_item, android.R.id.text1, names);
        boysList.setAdapter(adapter);
    }

    public void openPerms(View view) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = URL+"getPerms.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        fetchPerms(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "تعذر الاتصال", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        queue.add(postRequest);
    }

    private void fetchPerms(String response) {
        try
        {
            ArrayList<String> names = new ArrayList<>();
            JSONArray array = new JSONArray(response);
            for (int i=0; i<array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("boy_name");
                String date = object.getString("perm_date");
                String perm = object.getString("perm");
                names.add(name+"\n"+date+"\n"+perm);
            }
            populateList(names);

        }catch (Exception ex)
        {
            Toast.makeText(this, "لا تتوافر بيانات", Toast.LENGTH_SHORT).show();
            populateList(new ArrayList<String>());
        }
    }
}
