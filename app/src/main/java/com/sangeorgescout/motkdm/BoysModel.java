package com.sangeorgescout.motkdm;

public class BoysModel {
    public String id;
    public String name;
    public String phone;
    public String mobile;
    public String birthdate;
    public String grade;
    public String address;
    public String notes;
    public String image;
    public String fees;


    public BoysModel(String id, String name, String phone, String mobile, String birthdate, String grade, String address, String notes, String image, String fees) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.mobile = mobile;
        this.birthdate = birthdate;
        this.grade = grade;
        this.address = address;
        this.notes = notes;
        this.image = image;
        this.fees = fees;
    }
}
